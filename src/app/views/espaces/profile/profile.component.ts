import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UtilisateurService } from '../../../services/utilisateur/utilisateur.service';
import { ModalComponent } from '../../../shared/modal/modal/modal.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  inforPersoGroup: FormGroup
  @ViewChild("loadModal") public loadModal: ModalDirective

  constructor(private formBuilder: FormBuilder, private userService: UtilisateurService) {
    this.inforPersoGroup = formBuilder.group({
      nomUtilisateur: ["", Validators.required],
      emailUtilisateur: [""],
      telUtilisateur: [""],
      avatarUtilisateur: [""],
      role : {},
      naissanceUtilisateur: ["", Validators.required]
    })
  }

  // constructor() { }
  ngOnInit(): void {
    this.getUser()
  }


  saveInfo(val) {
    var modal = new ModalComponent()

    this.userService.modifyUserToServer(val.value).subscribe((u) => {
      // modal.hideModal()
      modal.showModal(true, "Mise à jour", "Vos informations sont mises à jour avec succès! " + val.value.email)
    }, (err) => {
      // modal.hideModal()
      var msg
      if (err.status == 0)
        msg = "Veuillez vérifier votre connexion internet"
      else if (err.status == 404)
        msg = "Aucune information trouvée pour l'adresse " + val.value.email
      else
        msg = "Veuillez vérifier vos identifiants"

      modal.showModal(false, "Echec de mise à jour!", msg)

      console.log("erreurRegister", err)
    })
  }

  getUser() {
    this.userService.getUtilisateurFromServer("ById", localStorage.getItem("utilisateurID")).subscribe((us) => {
      this.inforPersoGroup = this.formBuilder.group({
        nomUtilisateur: [us.nomUtilisateur, Validators.required],
        emailUtilisateur: [us.emailUtilisateur, Validators.email],
        telUtilisateur: [us.telUtilisateur, Validators.required],
        avatarUtilisateur: [us.avatarUtilisateur],
        role : us.role.nomRole,
        naissanceUtilisateur: [us.naissanceUtilisateur, Validators.required]
      })
      console.log(us)
    })
  }

}
