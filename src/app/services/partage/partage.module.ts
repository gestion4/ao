import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { BrowserModule } from '@angular/platform-browser';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal'
import { ErrorComponent } from '../../shared/modal/error/error.component';
import { LoaderComponent } from '../../shared/modal/loader/loader.component';
import { ModalComponent } from '../../shared/modal/modal/modal.component';
import { SuccessComponent } from '../../shared/modal/success/success.component';
import { MasterPageComponent } from '../../views/publics/master-page/master-page.component';
import { ListOffreComponent } from '../../views/publics/list-offre/list-offre.component';
import { TextMaskModule } from "angular2-text-mask"


@NgModule({
  declarations: [
    SuccessComponent,
    ErrorComponent,
    MasterPageComponent,
    ListOffreComponent,
    LoaderComponent, ModalComponent
  ],
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    FormsModule,
    ModalModule
  ],
  exports: [
    // CommonModule,
    BsDropdownModule,
    TabsModule,
    ChartsModule,
    ModalModule,
    MasterPageComponent,
    ListOffreComponent,
    FormsModule,
    SuccessComponent,
    ErrorComponent,
    LoaderComponent,
    ModalComponent,
    TextMaskModule

  ]
})
export class PartageModule { }
