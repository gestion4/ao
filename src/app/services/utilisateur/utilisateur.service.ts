import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  urlpath = 'api/v1/';
  constructor(private http: HttpClient, public baseUrl: BaseUrlService) {

  }
  public getUtilisateurFromServer(condition: 'All' | 'ById', value): Observable<any> {
    console.log("valuesession", value)
    let url = this.urlpath
    switch (condition) {
      case "ById":
        url += "utilisateur/" + value
        break;
      default:
        break;
    }

    return HttpService.httpGet(url, this.http)
  }

  public registerUserToServer(user): Observable<any> {
    var url = this.urlpath+"signup"
    return HttpService.httpPost({
      http: this.http,
      url: url
    }, user)
  }
  
  public modifyUserToServer(user): Observable<any> {
    var url = this.urlpath + "/" + user.participerID
    return HttpService.httpPut({
      http: this.http,
      url: url
    }, user)
  }
}
