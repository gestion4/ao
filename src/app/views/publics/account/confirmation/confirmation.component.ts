import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthentificationService } from '../../../../services/authentification/authentification.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {
  href: any
  title: string
  message: string
  btn_desc: string;
  key: string;
  showMessage: boolean = false
  constructor(private route: ActivatedRoute, private authService: AuthentificationService) {
    this.key = this.route.snapshot.paramMap.get("key")
    this.confirmEmail(this.key)
    console.log(this.key)

  }


  ngOnInit(): void {

  }
  ngAfterViewInit() {

    this.title = "Compte validé!"
    this.message = 'Votre compte a été activé avec succès!'
    this.btn_desc = "Voir mon profil"
    // document.getElementById("header").classList.add("bg-success")

  }

  confirmEmail(key) {
    //  this.href = "#confModal"
    this.authService.confirmEmail(key).subscribe((res) => {
      console.log(res)
      this.showMessage = true
      this.showSuccess()
    }, (err) => {
      this.showMessage = true
      console.log(err)
      this.showModal()
    })
  }
  showSuccess() {

    this.title = "Félicitation!"
    this.message = 'Votre compte a été activé avec succès!'
    this.btn_desc = "Voir mon profil"
    // document.getElementById("header").classList.add("bg-success")
    // document.getElementById("btn-showModal").click()
  }
  showModal() {

    this.title = "Echec de validation!"
    this.message = 'Votre compte n\'a pas été activé.'
    this.btn_desc = "Renvoyer un autre lien"
    // document.getElementById("header").classList.add("bg-danger")
    // document.getElementById("btn-showModal").click()
  }

}
