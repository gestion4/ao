import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { P404Component } from './../../views/publics/error/404.component';
import { P500Component } from './../../views/publics/error/500.component';
import { ConfirmationComponent } from './account/confirmation/confirmation.component';
import { LoginComponent } from './account/login/login.component';
import { PassOublieComponent } from './account/pass-oublie/pass-oublie.component';
import { RegisterComponent } from './account/register/register.component';
import { ResetMailComponent } from './account/reset-mail/reset-mail.component';

import { OffresComponent } from './offres/offres.component';


const routes : Routes = [
    { path : "", data : { title : "Public" }, children : [
        { path : "", redirectTo : "login" },
        { path : "offres", component : OffresComponent, data : { title : "Les Offres" } },

        { path: '404', component: P404Component, data: { title: 'Page 404' } },
        { path: '500', component: P500Component, data: { title: 'Page 500' } },
        { path: 'login', component: LoginComponent, data: { title: 'Page de connexion' } },
        { path: 'register', component: RegisterComponent, data: { title: 'Création de compte' } },
        { path: 'forgot-password', component: PassOublieComponent, data: { title: 'Réinitialisation de mot de passe' } },
        { path: 'reset-password', component: ResetMailComponent, data: { title: 'Nouveau mot de passe' } },
        { path: 'account/confirmation/:key', component: ConfirmationComponent, data: { title: 'Confirmation de compte' } }
    ] }
] 


@NgModule({
    imports : [RouterModule.forChild(routes)],
    exports : [RouterModule]
})
export class PublicsRoutingModule {}