// import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Ng5SliderModule } from 'ng5-slider';
import { NgSelectModule } from '@ng-select/ng-select';

//Cette import de service permet d'ajouter le # dans l'url
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

//Chargement du compoment de démarrage
import { AppComponent } from './app.component';


// Import routing module
import { AppRoutingModule } from './app.routing';  //C'est ici que les autre module sont chargé, a l'appel de la route correspondante
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  imports: [
    // BrowserModule,
    NgSelectModule,
    Ng5SliderModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,


  ],

  declarations: [AppComponent],

  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  // exports: [
  //   PartageModule
  // ],
  bootstrap: [AppComponent]
})
export class AppModule { }
