import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UtilisateurService } from '../../../../services/utilisateur/utilisateur.service';
import { ModalComponent } from '../../../../shared/modal/modal/modal.component';

@Component({
  selector: 'app-identite-view',
  templateUrl: './identite-view.component.html',
  styleUrls: ['./identite-view.component.css']
})
export class IdentiteViewComponent implements OnInit {

  @ViewChild("loadModal") public loadModal: ModalDirective

  inforPersoGroup: FormGroup
  constructor(private formBuilder: FormBuilder, private userService: UtilisateurService) {
    this.inforPersoGroup = formBuilder.group({
      nom: ["", Validators.required],
      contact: [""],
      dateNaissance: ["", Validators.required]
    })
  }

  ngOnInit(): void {

  }

  saveInfo(val: FormGroup) {
    var modal = new ModalComponent()
    this.loadModal.show()
    this.userService.modifyUserToServer(val.value).subscribe((u) => {
      // modal.hideModal()
      this.loadModal.hide()
      modal.showModal(true, "Mise à jour", "Vos informations sont mises à jour avec succès! " + val.value.email)
    }, (err) => {
      // modal.hideModal()
      var msg
      if (err.status == 0)
        msg = "Veuillez vérifier votre connexion internet"
      else if (err.status == 404)
        msg = "Aucune information trouvée pour l'adresse " + val.value.email
      else
        msg = "Veuillez vérifier vos identifiants"

      this.loadModal.hide()
      modal.showModal(false, "Echec de mise à jour!", msg)

      console.log("erreurRegister", err)
    })
  }

}
