import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfosComplementairesViewComponent } from './infos-complementaires-view.component';

describe('InfosComplementairesViewComponent', () => {
  let component: InfosComplementairesViewComponent;
  let fixture: ComponentFixture<InfosComplementairesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfosComplementairesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfosComplementairesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
