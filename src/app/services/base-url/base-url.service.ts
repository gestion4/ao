import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseUrlService {

  constructor() { }

  public getUrl() {
    //  return 'http://192.168.43.161:8080/'
    // return 'http://3.21.227.164:8080/';
    return 'http://192.168.1.31:8085/';
    return 'http://127.0.0.1:8080/';
  }

  public getCartoUrl() {
    // return 'http://192.168.43.161:8080/'
    // return 'http://192.168.8.100:8081/';
    return 'http://192.168.1.104:8080/';
  }

}