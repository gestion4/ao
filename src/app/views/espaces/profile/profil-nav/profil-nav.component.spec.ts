import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilNavComponent } from './profil-nav.component';

describe('ProfilNavComponent', () => {
  let component: ProfilNavComponent;
  let fixture: ComponentFixture<ProfilNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
