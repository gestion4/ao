import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  
  //Quand l'adresse du site est rentré, on est redirigé vers public
  { path: '', redirectTo: 'public', pathMatch: 'full', },
  
  //Chargement du module public, les composant nécéssaire pour cette route
  { path : '', data : { title : 'Public' }, children : [
    { path: 'public', loadChildren: () => import("./views/publics/publics.module").then(m => m.PublicsModule) } //Chargement du module à l'appel de la route
  ] },

  //Chargement du module espace, les composant nécéssaire pour cette route
  { path: '', data: { title: 'Espace' }, children: [ 
    { path: 'espace', loadChildren: () => import('./views/espaces/espaces.module').then(m => m.EspacesModule) },  //Chargement du module à l'appel de la route
  ]},

  // { path: '**', component: P404Component }
  { path: '**', redirectTo : '/public/404' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
