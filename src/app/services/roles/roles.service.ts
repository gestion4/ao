import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient, private baseUrl: BaseUrlService) {

  }

  public getRoles(): Observable<any> {
    // var header = new HttpHeaders()
    return this.http.get(this.baseUrl.getUrl() + 'api/v1/role')

  }
  

}
