import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AuthentificationService } from '../../../../services/authentification/authentification.service';
import { ModalComponent } from '../../../../shared/modal/modal/modal.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['../../../../input.css', './login.component.css']
})
export class LoginComponent {
  @ViewChild("loadModal") public loadModal: ModalDirective
  loginGroup: FormGroup
  constructor(private formBuilder: FormBuilder, private authService: AuthentificationService, private router: Router) {
    this.loginGroup = this.formBuilder.group({
      emailUtilisateur: ["", Validators.email],
      mdpUtilisateur: ["", Validators.required]
    })
  }

  ngOnInit(): void {
    var aut = this.authService.getUser()
    console.log(JSON.stringify(aut))
    if (aut != null && aut != undefined)
      this.router.navigate(["/espace"])

  }
  gotoRegister() {
    this.router.navigate(["/public/register"])
  }
  login(val: FormGroup) {
    var modal = new ModalComponent()
    console.log(modal)
    this.loadModal.show()
    this.authService.loginUser(val.value).subscribe((u) => {
      console.log(u)
      this.loadModal.hide()
      this.authService.insertToLocal(u)
      this.router.navigate(["/espace"], {
        replaceUrl: true
      })
    }, (err) => {
      // console.log(err)
      var msg
      this.loadModal.hide()
      if (err.status == 0)
        msg = "Le service n'est pas disponible"
      else if (err.status == 404)
        msg = "Identifications incorrects"
      else if (err.status == 401)
        msg = "Ce compte n'est pas activé! Veuillez vérifier votre boite mail"
      else
        msg = "Veuillez vérifier vos identifiants"

      // modal.hideModal()
      modal.showModal(false, "Echec de connexion", msg)

      console.log("erreurRegister", err)
    })
    console.log(val)
  }
}
