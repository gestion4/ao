import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbeViewComponent } from './abe-view.component';

describe('AbeViewComponent', () => {
  let component: AbeViewComponent;
  let fixture: ComponentFixture<AbeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
