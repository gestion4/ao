import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common";

import { EspacesRoutingModule } from './espaces-routing.module';

import { PartageModule } from './../../services/partage/partage.module';
// import { BrowserModule } from '@angular/platform-browser';


//Utilisation de Perfect Scrollbar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';


// Import containers
import { DefaultLayoutComponent } from './../../containers';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';
import { EditExperienceComponent } from './profile/experience-view/edit-experience/edit-experience.component';


// Import 3rd party components (Ils sont en commentaire parce qu'ils sont récupérer dans le module partage)
// import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// import { TabsModule } from 'ngx-bootstrap/tabs';
// import { ChartsModule } from 'ng2-charts';



@NgModule({
    imports : [
        EspacesRoutingModule,
        PerfectScrollbarModule,
        AppAsideModule,
        AppBreadcrumbModule.forRoot(),
        AppHeaderModule,
        AppFooterModule,
        AppSidebarModule,
        
        // BsDropdownModule.forRoot(),
        // TabsModule.forRoot(),
        // ChartsModule,


        PartageModule,
        CommonModule,
        // BrowserModule,
        // BrowserAnimationsModule,
    ],

    declarations : [
      ...APP_CONTAINERS,
    
     
    ],

    // providers : [{
    //   provide: LocationStrategy,
    //   useClass: HashLocationStrategy
    // }]
})
export class EspacesModule { }