import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from '../../../../services/utilisateur/utilisateur.service';

@Component({
  selector: 'app-identite',
  templateUrl: './identite.component.html',
  styleUrls: ['./identite.component.css']
})
export class IdentiteComponent implements OnInit {

  constructor(private userService : UtilisateurService) { }
  userInfo : any
  ngOnInit(): void {
    this.userInfo ={}
    this.getUser()
  }

  getUser() {
    this.userService.getUtilisateurFromServer("ById", localStorage.getItem("utilisateurID")).subscribe((us) => {
      this.userInfo = us
      // {
      //   nomUtilisateur: [us.nomUtilisateur, Validators.required],
      //   emailUtilisateur: [us.emailUtilisateur, Validators.email],
      //   telUtilisateur: [us.telUtilisateur, Validators.required],
      //   avatarUtilisateur: [us.avatarUtilisateur],
      //   role : us.role.nomRole,
      //   dateNaissance: ["", Validators.required]
      // }
      console.log(us)
    })
  }
}
