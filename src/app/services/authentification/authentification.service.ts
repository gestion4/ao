import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  url = 'api/v1/'
  constructor(private http: HttpClient, private baseUrl: BaseUrlService) {

  }

  public loginUser(data: { emailUtilisateur, mdpUtilisateur }): Observable<any> {
    var header = new HttpHeaders()
    return this.http.post(this.baseUrl.getUrl() + this.url + 'signin', data)

  }
  public confirmEmail(key): Observable<any> {
    return this.http.get(this.baseUrl.getUrl()+this.url+"utilisateur/validation/"+key)
  }

  public passOublie(email): Observable<any> {
    return this.http.get(this.baseUrl.getUrl() +this.url+ "utilisateur/reinitialisation/" + email, {})
  }

  public insertToLocal(data) {

    localStorage.setItem("utilisateurID", data.utilisateurID)
    localStorage.setItem("avatarUtilisateur", data.avatarUtilisateur)
    localStorage.setItem("emailUtilisateur", data.emailUtilisateur)
    localStorage.setItem("isActive", data.isActive)
    localStorage.setItem("isValide", data.isValide)
    localStorage.setItem("mdpUtilisateur", data.mdpUtilisateur)
    localStorage.setItem("nomUtilisateur", data.nomUtilisateur)
    localStorage.setItem("role", data.role.nomRole,)
    localStorage.setItem("telUtilisateur", data.telUtilisateur)
  }
  public getUser() {
    return localStorage.getItem("utilisateurID")
  }
}
