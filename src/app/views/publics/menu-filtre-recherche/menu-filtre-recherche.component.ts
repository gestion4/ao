import { Component, OnInit } from '@angular/core';
// import * as $ from 'jquery';
import { Options, LabelType } from 'ng5-slider';

@Component({
  selector: 'app-menu-filtre-recherche',
  templateUrl: './menu-filtre-recherche.component.html',
  styleUrls: ['./menu-filtre-recherche.component.scss']
})
export class MenuFiltreRechercheComponent implements OnInit {
  selectedCityIds: any
  fields = [
    { id: 2, name: 'Informatique' },
  ]

  places = [
    { id: 1, name: 'Abengourou' },
    { id: 1, name: 'Abidjan' },
  ]
  grades = [
    { id: 2, name: 'BAC +1' },
    { id: 3, name: 'BAC +2', /*disabled: true*/ },
    { id: 4, name: 'BAC +3' },
    { id: 5, name: 'BAC +4' },
    { id: 6, name: 'BAC +5' },
    { id: 7, name: 'BAC +6' },
    { id: 8, name: 'BAC +7 et plus' },
  ];

  datesPub = [
    { id: 1, name: 'Aujourd\'hui' },
    { id: 1, name: 'Moins de 3 jours' },
    { id: 1, name: 'Moins de 1 semaine' },
    { id: 1, name: 'Moins de 2 semaines' },
    { id: 1, name: 'Moins de 1 mois' },
    { id: 1, name: 'Moins de 3 mois' },
  ]
  dateId: any

  minValue: number = 1;
  maxValue: number = 50;
  options: Options = {
    floor: 1,
    ceil: 50,
    translate: (value: number, label: LabelType): string => {
      var l = "an"
      if (value > 1)
        l += 's'

      return value + ' <b>' + l + '</b>';

      // switch (label) {
      //   case LabelType.Low:
      //     return value + '<b>' + l + '</b>';
      //   case LabelType.High:
      //     return '<b>Max price:</b> $' + value;
      //   default:
      //     return '$' + value;
      // }
    }
  };


  constructor() { }

  ngOnInit(): void {
    // $("#menu-toggle").click(function (e) {
    //   e.preventDefault();
    //   $("#wrapper").toggleClass("toggled");
    // });
  }

}
