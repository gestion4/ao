import { Injectable } from '@angular/core';
import { BaseUrlService } from '../base-url/base-url.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor() { }
  static header = {
    append: [
      'Content-Type', 'application/json',
      'Access-Control-Allow-Methods', 'GET, POST',
      'Access-Control-Allow-Credentials', 'true',
      'Access-Control-Allow-Headers', 'location',
      // "Accept", "*/*",
      // 'Access-Control-Allow-Origin', '*'
    ]
  }
  static httpGet(url, http : HttpClient) {
    return http.get(new BaseUrlService().getUrl() + url, {
      headers: HttpService.header,
      // responseType: 'text',
    })
  }
  static httpPost(config: { http, url }, data, type? :'file' | 'default') {
    var hder
    if (type == "file") {
      hder = {
        append: [
          'Content-Type', "multipart/form-data;",
          'Access-Control-Allow-Methods', 'GET, POST',
          'Access-Control-Allow-Credentials', 'true',
          'Access-Control-Allow-Headers', 'location',
          // 'Authorization', 'Bearer '+UtilitiesService.loggedUser.token
        ]
      }
    } else
      hder = HttpService.header
    return config.http.post(new BaseUrlService().getUrl() + config.url, data, {
      headers: hder,
      responseType: 'json',
      observe: 'response'
    })
  }
  static httpPut(config: { http: HttpClient, url }, data) {
    // HttpService.header.append.push('Authorization', 'Bearer ' + UtilitiesService.loggedUser.token)
    return config.http.put(new BaseUrlService().getUrl() + config.url, data, {
      headers: HttpService.header,
      responseType: 'json',
      observe: 'response'
    })
  }
}
