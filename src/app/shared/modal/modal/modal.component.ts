import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  // styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @ViewChild("loadModal") public loadModal: ModalDirective

  constructor() {

  }
  ngAfterViewInit() {

  }
  ngOnInit(): void {
  }

  hide(btn) {
    document.getElementById(btn).click()

  }
  showModal(error: boolean, title, message) {
    var ttle
    var msg

    let btn
    if (!error) {
      btn = "btnSubmitError"
      ttle = "err-title"
      msg = "err-message"
    }
    else {
      btn = "btnSubSuccess"
      ttle = "suc-title"
      msg = "suc-message"
    }
    document.getElementById(ttle).innerText = title
    document.getElementById(msg).innerText = message
    document.getElementById(btn).click()

  }

}
