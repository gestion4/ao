import { NgModule } from '@angular/core';

// import { FormsModule } from '@angular/forms';
// import { ChartsModule } from 'ng2-charts';
// import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { PartageModule } from './../../../services/partage/partage.module';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ProfileComponent } from '../profile/profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExperiencesComponent } from '../cv-view/experiences/experiences.component';
import { IdentiteComponent } from '../cv-view/identite/identite.component';
import { CvViewComponent } from '../cv-view/cv-view.component';
import { AbeComponent } from '../cv-view/abe/abe.component';
import { IdentiteViewComponent } from '../profile/identite-view/identite-view.component';
import { ExperienceViewComponent } from '../profile/experience-view/experience-view.component';
import { AbeViewComponent } from '../profile/abe-view/abe-view.component';
import { ProfilNavComponent } from '../profile/profil-nav/profil-nav.component';
import { ResetPasswordComponent } from '../profile/reset-password/reset-password.component';
import { FormationsViewComponent } from '../profile/formations-view/formations-view.component';
import { InfosComplementairesViewComponent } from '../profile/infos-complementaires-view/infos-complementaires-view.component';
import { EditExperienceComponent } from '../profile/experience-view/edit-experience/edit-experience.component';

@NgModule({
  imports: [
    // FormsModule,
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // ChartsModule,
    // BsDropdownModule,

    ButtonsModule.forRoot(), PartageModule
  ],
  declarations: [
    DashboardComponent,
    IdentiteViewComponent,
    ProfileComponent,
    AbeComponent,
    ResetPasswordComponent,
    CvViewComponent,
    ProfilNavComponent,
    IdentiteComponent,
    ExperiencesComponent,
    IdentiteComponent,
    ExperienceViewComponent,
    AbeViewComponent,
    FormationsViewComponent,
    InfosComplementairesViewComponent,
    EditExperienceComponent,



  ]
})
export class DashboardModule { }
