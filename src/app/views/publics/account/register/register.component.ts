import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { RolesService } from '../../../../services/roles/roles.service';
import { UtilisateurService } from '../../../../services/utilisateur/utilisateur.service';
import { ModalComponent } from '../../../../shared/modal/modal/modal.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html',
  styleUrls: ["../../../../shared/modal/modal.scss", '../../../../input.css', './register.component.css']
})
export class RegisterComponent {
  hidden = false
  registerGroup: FormGroup
  @ViewChild("loadModal") public loadModal: ModalDirective
  @ViewChild("successModal") public successModal: ModalDirective
  modal = new ModalComponent()
  private roles: any[]
  passNomatched: boolean;
  adresse: any
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UtilisateurService, private roleService: RolesService) {

    this.roles = []
    this.registerGroup = this.formBuilder.group({
      typeCompte: ['', Validators.required],
      nomUtilisateur: ['', Validators.required],
      emailUtilisateur: ['', Validators.email],
      telUtilisateur: [''],
      // naissanceUtilisateur: ['', Validators.required],
      mdpUtilisateur: ['', Validators.required],
      passconf: ['', Validators.required],
      role: {
        roleID: null
      },
      condition: [false, Validators.requiredTrue],
    })


  }
  ngOnInit() {
    this.getRole()
  }
  ngAfterViewInit() {
  }
  register(val: FormGroup) {
    // this.successModal.show()
    // return
    this.loadModal.show()
    console.log(val.value)
    if (val.value.mdpUtilisateur != val.value.passconf) {
      this.passNomatched = true
      return
    }
    this.successModal.config = {
      backdrop: "static"
    }

    val.value.role.roleID = val.value.typeCompte
    var scope = this
    this.userService.registerUserToServer(val.value).subscribe((u) => {
      this.loadModal.hide()
      this.successModal.show()

    }, (err) => {
      this.loadModal.hide()
      var msg = "La création du compte a echoué!"
      if (err.status == 500)
        msg = err.error.message
      this.modal.showModal(false, "Echec d'enregistrement", msg)
      console.log("erreurRegister", err)
    })
  }
  getRole() {
    this.roleService.getRoles().subscribe((rl: any[]) => {
      // delete rl[0]
      rl.splice(0, 1)
      this.roles = rl

      console.log(rl)
    }, (err) => {
      this.modal.showModal(false, "Création de compte", "Erreur de chargement des données")
    })
  }

  close() {
    this.loadModal.hide()
    // this.modal.hide("btnSubmitError")
    this.successModal.hide()
    setTimeout(() => {
      this.router.navigate([""])
    }, 1000)
  }



}
