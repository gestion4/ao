import { NgModule } from "@angular/core"
import { Routes, RouterModule } from '@angular/router';


// import { TabBordComponent } from './tab-bord/tab-bord.component';
import { DefaultLayoutComponent } from './../../containers';



const routes : Routes = [
    { path : "", component: DefaultLayoutComponent, data : { title : "Espace" }, children : [  
    // { path : "", data : { title : "Espace" }, children : [  
        { path : "", redirectTo : "dashboard" },
        { path : "dashboard", loadChildren : () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
    ] }
]


@NgModule({
    imports : [RouterModule.forChild(routes)],
    exports : [RouterModule]
})
export class EspacesRoutingModule {}