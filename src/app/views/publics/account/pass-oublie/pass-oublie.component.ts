import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AuthentificationService } from '../../../../services/authentification/authentification.service';
import { ModalComponent } from '../../../../shared/modal/modal/modal.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './pass-oublie.component.html',
  styleUrls: ['../../../../input.css','./pass-oublie.component.css']
})
export class PassOublieComponent implements OnInit {

  @ViewChild("loadModal") public loadModal: ModalDirective
  passGroup: FormGroup
  constructor(private formBuilder: FormBuilder, private authService: AuthentificationService) {
    this.passGroup = this.formBuilder.group({
      emailUtilisateur: ["", Validators.email],
    })
  }

  reset(val: FormGroup) {
    var modal = new ModalComponent()
    this.loadModal.show()
    console.log(val.value, val.value.emailUtilisateur)
    this.authService.passOublie(val.value.emailUtilisateur).subscribe((u) => {
      console.log(u)
      // modal.hideModal()
      this.loadModal.hide()
      modal.showModal(true, "Réinitialisation", "Un mail de réinitialisation de mot de passe a été envoyé à l'adresse " + val.value.emailUtilisateur +
        "\nIl contient un lien sur lequel il vous faudra cliquer afin de réinitialiser votre mot de passe.")
    }, (err) => {
      // modal.hideModal()
      this.loadModal.hide()
      var msg
      if (err.status == 0)
        msg = "Le service n'est pas disponible."
      else (err.status == 404)
        msg = "Aucune information trouvée pour l'adresse " + val.value.emailUtilisateur
      // else
      //   msg = "Veuillez vérifier vos identifiants"

      modal.showModal(false, "Echec réinitialisation", msg)
      console.log("erreurRegister", err)
    })
  }
  ngOnInit(): void {
  }

}
