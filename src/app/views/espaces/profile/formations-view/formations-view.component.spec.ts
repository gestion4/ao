import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationsViewComponent } from './formations-view.component';

describe('FormationsViewComponent', () => {
  let component: FormationsViewComponent;
  let fixture: ComponentFixture<FormationsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormationsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
