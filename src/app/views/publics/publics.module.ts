import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import { PublicsRoutingModule } from './publics-routing.module';


import { P404Component } from './../../views/publics/error/404.component';
import { P500Component } from './../../views/publics/error/500.component';

import { OffresComponent } from './offres/offres.component';
import { MasterPageComponent } from './master-page/master-page.component';
import { ListOffreComponent } from './list-offre/list-offre.component';
import { MenuFiltreRechercheComponent } from './menu-filtre-recherche/menu-filtre-recherche.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { Ng5SliderModule } from 'ng5-slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PartageModule } from '../../services/partage/partage.module';
import { ConfirmationComponent } from './account/confirmation/confirmation.component';
import { LoginComponent } from './account/login/login.component';
import { PassOublieComponent } from './account/pass-oublie/pass-oublie.component';
import { RegisterComponent } from './account/register/register.component';
import { ResetMailComponent } from './account/reset-mail/reset-mail.component';



@NgModule({
    declarations: [
        P404Component,
        P500Component,
        LoginComponent,
        RegisterComponent,
        // MasterPageComponent,
        // ListOffreComponent,
        MenuFiltreRechercheComponent,
        OffresComponent,
        PassOublieComponent,
        ConfirmationComponent,
        ResetMailComponent,
        // ModalComponent,
        // SuccessComponent,
        // ErrorComponent,
        // LoaderComponent
    ],

    imports: [CommonModule, PublicsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        Ng5SliderModule, 
        PartageModule
    ]
})
export class PublicsModule { }