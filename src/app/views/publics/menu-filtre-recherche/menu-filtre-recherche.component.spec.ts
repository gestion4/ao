import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuFiltreRechercheComponent } from './menu-filtre-recherche.component';

describe('MenuFiltreRechercheComponent', () => {
  let component: MenuFiltreRechercheComponent;
  let fixture: ComponentFixture<MenuFiltreRechercheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuFiltreRechercheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuFiltreRechercheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
