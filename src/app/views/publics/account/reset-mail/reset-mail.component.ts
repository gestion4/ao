import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-reset-mail',
  templateUrl: './reset-mail.component.html',
  styleUrls: ['../../../../input.css','./reset-mail.component.css']
})
export class ResetMailComponent implements OnInit {
  inforPersoGroup:FormGroup
  constructor(private formBuilder: FormBuilder) { 
    this.inforPersoGroup = this.formBuilder.group({
      
    })
  }

  ngOnInit(): void {
  }

}
