import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-offre',
  templateUrl: './list-offre.component.html',
  styleUrls: ['./list-offre.component.scss']
})
export class ListOffreComponent implements OnInit {
  items = []
  constructor() { }

  ngOnInit(): void {
    this.items = [0, 1, 2, 3, 4, 0, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0]
  }
  getIcon(i) {
    return i % 2 == 0 ? "/assets/img/ao-b.jpg" : "/assets/img/ao-r.jpg"
  }
}
