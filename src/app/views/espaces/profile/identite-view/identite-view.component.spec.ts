import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentiteViewComponent } from './identite-view.component';

describe('IdentiteViewComponent', () => {
  let component: IdentiteViewComponent;
  let fixture: ComponentFixture<IdentiteViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentiteViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentiteViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
